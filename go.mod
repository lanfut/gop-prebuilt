module app

go 1.17

require github.com/goplus/gop v1.0.16

require (
	github.com/goplus/gox v1.7.10 // indirect
	github.com/qiniu/x v1.11.5 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/tools v0.1.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
